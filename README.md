# Reinforcement learning algorithms

This repository contains simple implementations of reinforcement learning algorithms.

# Installation instructions

The installation instructions are as follows (tested on a Linux system): 

0. Clone this repository using
```commandline
git clone https://github.com/jwergieluk/rl.git
```
1. Install Anaconda Python distribution: https://www.anaconda.com/distribution/#download-section
2. Create a virtual environment with all the necessary packages and activate it:
```commandline
conda create -n rl_gym -c conda-forge -c pytorch -c likan999 python box2d-py pyglet cloudpickle pytorch torchvision cudatoolkit=10.0 numpy scipy pandas matplotlib opencv six
```
3. Clone and install OpenAI gym:
```commandline
git clone git@github.com:openai/gym.git /tmp/gym
cd /tmp/gym
pip install -e .[box2d]
```

# Environments 

Pendulum-v0
LunarLanderContinuous-v2



