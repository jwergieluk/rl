import time

import torch
import torch.nn
import torch.optim
import numpy
import click
import tensorboardX
import gym
import gym.spaces
from baselines.common import atari_wrappers
import cv2
cv2.ocl.setUseOpenCL(False)


ENV_NAME = "PongNoFrameskip-v4"


class ScaledFloatFrame(gym.ObservationWrapper):
    def __init__(self, env):
        gym.ObservationWrapper.__init__(self, env)
        self.observation_space = gym.spaces.Box(low=0, high=1, shape=env.observation_space.shape, dtype=np.float32)

    def observation(self, observation):
        # careful! This undoes the memory optimization, use
        # with smaller replay buffers only.
        return np.array(observation).astype(np.float32) / 255.0


class TransformFrame(gym.ObservationWrapper):
    def __init__(self, env, width=84, height=84):
        """Warp frames to 84x84 as done in the Nature paper and later work."""
        gym.ObservationWrapper.__init__(self, env)
        self.width = width
        self.height = height
        self.observation_space = gym.spaces.Box(low=0, high=255, shape=(self.height, self.width, 1), dtype=numpy.uint8)

    def observation(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        frame = cv2.resize(frame, (self.width, self.height), interpolation=cv2.INTER_AREA)
        frame = numpy.expand_dims(frame, -1).astype(numpy.float32)/255.0
        return frame


def make_env_train(env_name: str):
    env = gym.make(env_name)
    env = atari_wrappers.EpisodicLifeEnv(env)
    env = atari_wrappers.NoopResetEnv(env)   # take random number of no-ops on reset
    env = atari_wrappers.MaxAndSkipEnv(env, skip=4)   # Return only every `skip`-th frame
    env = atari_wrappers.FireResetEnv(env)   # Take action on reset for environments that are fixed until firing
    env = TransformFrame(env)
    # env = atari_wrappers.WarpFrame(env, height=48, width=48)   # Scale down to
    # env = atari_wrappers.FrameStack(env, 4)   # Stack k last frames
    return env


def main():
    env = make_env_train(ENV_NAME)
    state = env.reset()
    cv2.namedWindow('window', cv2.WINDOW_NORMAL)

    while True:
        state, reward, done, _ = env.step(env.action_space.sample())
        print(reward, state.shape, state.dtype)
        cv2.imshow('window', numpy.squeeze(state))
        if done:
            break
        time.sleep(0.5)


if __name__ == '__main__':
    main()

